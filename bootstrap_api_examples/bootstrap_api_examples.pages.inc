<?php

/**
 * Page Callback: Example overview page
 *
 * @return array
 */
function bootstrap_api_examples_page() {
  $element = array();

  $element['modal-link'] = array(
    '#type' => 'link',
    '#title' => t('Open modal'),
    '#href' => "admin/config/user-interface/bootstrap-api/examples/modal/nojs",
    '#ajax' => array(
      'path' => "admin/config/user-interface/bootstrap-api/examples/modal/ajax",
    ),
    '#attached' => array(
      'library' => array(
        array('bootstrap_api', 'bootstrap-api.modal')
      ),
    ),
    '#options' => array(
      'query' => drupal_get_destination(),
      'html' => TRUE,
      'attributes' => array(
        'class' => array('btn', 'btn-primary'),
      ),
    ),
  );

  return array(
    '#theme' => 'bootstrap_api_examples_overview',
    'elements' => $element,
  );
}

/**
 * Page Callback: Simple Modal Page Content: delivers only a renderable markup array in this case...
 * @return array
 */
function bootstrap_api_examples_page_modal() {
  return array(
    '#markup' => '
        <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin sodales venenatis ipsum, sit amet sodales orci sagittis ac. Nunc pretium ante a posuere euismod. Nulla vestibulum dictum justo vitae ullamcorper. Nam pharetra ut nisl et malesuada. Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque fringilla sem id sem fermentum mattis. Praesent sit amet dolor viverra, lobortis erat at, venenatis arcu. Cras porta tincidunt nunc, nec vestibulum purus varius porttitor. Vestibulum interdum elit sollicitudin, malesuada enim vel, condimentum nulla. Quisque non nibh faucibus, mollis eros vitae, consequat massa. Vivamus tincidunt massa et scelerisque imperdiet. Mauris a accumsan nisi, ac pellentesque quam. Proin scelerisque, est vitae fringilla gravida, arcu dolor ultricies mauris, dapibus fermentum est diam eu nunc.</p>
        <p>In vel sollicitudin mauris. In facilisis bibendum dui, non pellentesque massa dignissim non. Fusce ullamcorper tortor id feugiat fringilla. Aenean in dictum nisi. Nulla adipiscing ornare justo blandit tincidunt. Nunc vel purus nec lacus pulvinar posuere. Pellentesque molestie velit tortor, et venenatis arcu sagittis quis.</p>'
  );
}

/**
 * Paga Callback: used as ajax wrapper for content delivery
 * @return array
 */
function bootstrap_api_examples_page_modal_ajax_callback() {
  bootstrap_api_modal_display(TRUE);

  // Simple get content from original page callback
  $content = bootstrap_api_examples_page_modal();

  $commands = array();
  $commands[] = bootstrap_api_command_modal('show', $content, array('title' => drupal_get_title()));

  return array('#type' => 'ajax', '#commands' => $commands);

}